package com.yoshiky.app.util;

public abstract class Constante {

	public abstract class PARAMETRO {

		public static final String COD_RESPUESTA_EXITO = "0";
		public static final String MSJ_RESPUESTA_EXITO = "Éxito";
		public static final String COD_RESPUESTA_NO_EXITO = "1";
		public static final String MSJ_RESPUESTA_NO_EXITO = "No Éxito";
		
	}
}
