package com.yoshiky.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yoshiky.app.bean.RespuestaBean;
import com.yoshiky.app.model.Venta;
import com.yoshiky.app.service.VentaService;
import com.yoshiky.app.util.Constante;

@RestController
@RequestMapping("/venta")
public class VentaController {

	private static final Logger log = LoggerFactory.getLogger(VentaController.class);

	@Autowired
	private VentaService ventaService;

	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaBean> registrar(@RequestBody Venta venta) {
		log.info("Inicia método regitrar venta.");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			ventaService.crear(venta);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);
		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Venta>> listar() {
		log.info("Inicia método listar ventas.");
		List<Venta> lstVentas = new ArrayList<Venta>();
		try {
			lstVentas = ventaService.encontrarTodos();
		} catch (Exception e) {
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(lstVentas, HttpStatus.OK);
	}

}
