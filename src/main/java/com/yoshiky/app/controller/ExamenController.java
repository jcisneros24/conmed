package com.yoshiky.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yoshiky.app.bean.RespuestaBean;
import com.yoshiky.app.model.Examen;
import com.yoshiky.app.service.ExamenService;
import com.yoshiky.app.util.Constante;

@RestController
@RequestMapping("/examen")
public class ExamenController {

	private static final Logger log = LoggerFactory.getLogger(ExamenController.class);

	@Autowired
	private ExamenService examenService;

	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaBean> registrar(@RequestBody Examen examen) {
		log.info("Inicia método registro del éxamen.");
		RespuestaBean respuesta = new RespuestaBean();
		try {

			examenService.crear(examen);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);

		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@PostMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaBean> actualizar(@RequestBody Examen examen) {
		log.info("Inicia método actualización del éxamen.");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			examenService.actualizar(examen);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);
		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@DeleteMapping(value = "/eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaBean> eliminar(@PathVariable("id") Integer id) {
		log.info("Inicia método eliminación del éxamen.");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			examenService.eliminarXId(id);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);
		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Examen>> listar() {
		log.info("Inicia método listar los éxamenes.");
		List<Examen> lstExamenes = new ArrayList<Examen>();
		try {
			lstExamenes = examenService.encontrarTodos();
		} catch (Exception e) {
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(lstExamenes, HttpStatus.OK);
	}

	@GetMapping(value = "/listar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Examen> listar(@PathVariable("id") Integer id) {
		log.info("Inicia método listar el éxamen.");
		Examen examen = new Examen();
		try {
			examen = examenService.encontrarXId(id);
		} catch (Exception e) {
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(examen, HttpStatus.OK);
	}

}
