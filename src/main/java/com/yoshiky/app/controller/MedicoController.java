package com.yoshiky.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yoshiky.app.bean.RespuestaBean;
import com.yoshiky.app.model.Medico;
import com.yoshiky.app.service.MedicoService;
import com.yoshiky.app.util.Constante;

@RestController
@RequestMapping("/medico")
public class MedicoController {

	private static final Logger log = LoggerFactory.getLogger(MedicoController.class);

	@Autowired
	private MedicoService medicoService;

	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaBean> registrar(@RequestBody Medico medico) {
		log.info("Inicia método de registro del médico.");
		RespuestaBean respuesta = new RespuestaBean();
		try {

			medicoService.crear(medico);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);

		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@PostMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaBean> actualizar(@RequestBody Medico medico) {
		log.info("Inicia método de actualización del médico.");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			medicoService.actualizar(medico);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);
		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@DeleteMapping(value = "/eliminar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaBean> eliminar(@RequestBody Medico medico) {
		log.info("Inicia método eliminación del médico.");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			medicoService.eliminar(medico);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);
		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Medico>> listar() {
		log.info("Inicia método listar los médicos.");
		List<Medico> lstMedicos = new ArrayList<Medico>();
		try {
			lstMedicos = medicoService.encontrarTodos();
		} catch (Exception e) {
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(lstMedicos, HttpStatus.OK);
	}

	@GetMapping(value = "/listar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Medico> listar(@PathVariable("id") Integer id) {
		log.info("Inicia método listar el médico.");
		Medico medico = new Medico();
		try {
			medico = medicoService.encontrarXId(id);
		} catch (Exception e) {
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(medico, HttpStatus.OK);
	}

}
