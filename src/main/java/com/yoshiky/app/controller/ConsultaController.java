package com.yoshiky.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yoshiky.app.bean.RespuestaBean;
import com.yoshiky.app.model.Consulta;
import com.yoshiky.app.service.ConsultaService;
import com.yoshiky.app.util.Constante;

@RestController
@RequestMapping("/consulta")
public class ConsultaController {

	private static final Logger log = LoggerFactory.getLogger(ConsultaController.class);

	@Autowired
	private ConsultaService consultaService;

	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaBean> registrar(@RequestBody Consulta consulta) {
		log.info("Inicia registro del consulta.");
		RespuestaBean respuesta = new RespuestaBean();
		try {

			consultaService.crear(consulta);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);

		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Consulta>> listar() {
		log.info("Inicia metodo listar los consultas.");
		List<Consulta> lstConsultas = new ArrayList<Consulta>();
		try {
			lstConsultas = consultaService.encontrarTodos();
		} catch (Exception e) {
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(lstConsultas, HttpStatus.OK);
	}

}
