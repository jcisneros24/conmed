package com.yoshiky.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yoshiky.app.bean.RespuestaBean;
import com.yoshiky.app.model.Producto;
import com.yoshiky.app.service.ProductoService;
import com.yoshiky.app.util.Constante;

@RestController
@RequestMapping("/producto")
public class ProductoController {

	private static final Logger log = LoggerFactory.getLogger(ProductoController.class);

	@Autowired
	private ProductoService productoService;

	@PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaBean> registrar(@RequestBody Producto producto) {
		log.info("Inicia registro del producto.");
		RespuestaBean respuesta = new RespuestaBean();
		try {

			productoService.crear(producto);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);

		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@PostMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaBean> actualizar(@RequestBody Producto producto) {
		log.info("Inicia actualización del producto.");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			productoService.actualizar(producto);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);
		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@DeleteMapping(value = "/eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaBean> eliminar(@PathVariable("id") Integer id) {
		log.info("Inicia eliminación del producto.");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			productoService.eliminarXId(id);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_EXITO);
		} catch (Exception e) {
			log.error("Error en DB: " + e);
			respuesta.setCodigo(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			respuesta.setMensaje(Constante.PARAMETRO.MSJ_RESPUESTA_NO_EXITO);
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(respuesta, HttpStatus.OK);
	}

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producto>> listar() {
		log.info("Inicia metodo listar los productos.");
		List<Producto> lstProductos = new ArrayList<Producto>();
		try {
			lstProductos = productoService.encontrarTodos();
		} catch (Exception e) {
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(lstProductos, HttpStatus.OK);
	}

	@GetMapping(value = "/listar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> listar(@PathVariable("id") Integer id) {
		log.info("Inicia metodo listar el producto.");
		Producto producto = new Producto();
		try {
			producto = productoService.encontrarXId(id);
		} catch (Exception e) {
			new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(producto, HttpStatus.OK);
	}

}
