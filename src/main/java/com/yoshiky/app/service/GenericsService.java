package com.yoshiky.app.service;

import java.util.List;

public interface GenericsService<T> {

	T crear(T t) throws Exception;

	T actualizar(T t) throws Exception;

	void eliminar(T id) throws Exception;
	
	void eliminarXId(int id) throws Exception;

	T encontrarXId(int id) throws Exception;

	List<T> encontrarTodos() throws Exception;

}
