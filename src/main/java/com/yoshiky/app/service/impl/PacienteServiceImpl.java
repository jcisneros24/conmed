package com.yoshiky.app.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yoshiky.app.dao.PacienteDao;
import com.yoshiky.app.dao.PersonaDao;
import com.yoshiky.app.model.Paciente;
import com.yoshiky.app.model.Persona;
import com.yoshiky.app.service.PacienteService;

@Service
public class PacienteServiceImpl implements PacienteService {

	private static final Logger log = LoggerFactory.getLogger(PacienteServiceImpl.class);

	@Autowired
	private PacienteDao pacienteDao;

	@Autowired
	private PersonaDao personaDao;

	@Override
	public Paciente crear(Paciente t) throws Exception {
		log.info("Inicia logica para crear un paciente...");
		Persona persona = t.getPersona();
		personaDao.save(persona);
		t.setPersona(persona);
		return pacienteDao.save(t);
	}

	@Override
	public Paciente actualizar(Paciente t) throws Exception {
		log.info("Inicia logica para actualizar un paciente...");
		Persona persona = t.getPersona();
		personaDao.save(persona);
		t.setPersona(persona);
		return t;
	}

	@Override
	public void eliminar(Paciente paciente) throws Exception {
		log.info("Inicia logica para eliminar por completo un paciente...");
		pacienteDao.delete(paciente);
		personaDao.delete(paciente.getPersona().getId());
	}

	@Override
	public Paciente encontrarXId(int id) throws Exception {
		log.info("Inicia logica para encontrar por ID un paciente...");
		return pacienteDao.findOne(id);
	}

	@Override
	public List<Paciente> encontrarTodos() throws Exception {
		log.info("Inicia logica para encontrar a todos los pacientes...");
		return pacienteDao.findAll();
	}

	@Override
	public void eliminarXId(int id) throws Exception {
		// TODO Auto-generated method stub

	}

}
