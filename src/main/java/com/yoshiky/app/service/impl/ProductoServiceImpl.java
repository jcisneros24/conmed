package com.yoshiky.app.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yoshiky.app.dao.ProductoDao;
import com.yoshiky.app.model.Producto;
import com.yoshiky.app.service.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService {

	private static final Logger log = LoggerFactory.getLogger(ProductoServiceImpl.class);

	@Autowired
	private ProductoDao productoDao;

	@Override
	public Producto crear(Producto t) throws Exception {
		log.info("Inicia lógica para crear un producto...");
		return productoDao.save(t);
	}

	@Override
	public Producto actualizar(Producto t) throws Exception {
		log.info("Inicia lógica para actualizar un producto...");
		return productoDao.save(t);
	}

	@Override
	public void eliminarXId(int id) throws Exception {
		log.info("Inicia lógica para eliminar un producto...");
		productoDao.delete(id);
	}

	@Override
	public Producto encontrarXId(int id) throws Exception {
		log.info("Inicia lógica para encontrar por ID un producto...");
		return productoDao.findOne(id);
	}

	@Override
	public List<Producto> encontrarTodos() throws Exception {
		log.info("Inicia lógica para encontrar a todos los products");
		return productoDao.findAll();
	}

	@Override
	public void eliminar(Producto id) throws Exception {
		// TODO Auto-generated method stub

	}

}
