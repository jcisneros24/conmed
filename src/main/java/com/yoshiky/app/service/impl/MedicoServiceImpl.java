package com.yoshiky.app.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yoshiky.app.dao.MedicoDao;
import com.yoshiky.app.dao.PersonaDao;
import com.yoshiky.app.model.Medico;
import com.yoshiky.app.model.Persona;
import com.yoshiky.app.service.MedicoService;

@Service
public class MedicoServiceImpl implements MedicoService {

	private static final Logger log = LoggerFactory.getLogger(MedicoServiceImpl.class);

	@Autowired
	private MedicoDao medicoDao;

	@Autowired
	private PersonaDao personaDao;

	@Override
	public Medico crear(Medico t) throws Exception {
		log.info("Inicia lógica para crear un médico...");
		Persona persona = t.getPersona();
		personaDao.save(persona);
		t.setPersona(persona);
		return medicoDao.save(t);
	}

	@Override
	public Medico actualizar(Medico t) throws Exception {
		log.info("Inicia lógica para actualizar un médico...");
		personaDao.save(t.getPersona());
		return t;
	}

	@Override
	public void eliminar(Medico medico) throws Exception {
		log.info("Inicia lógica para eliminar médico...");
		medicoDao.delete(medico);
		personaDao.delete(medico.getPersona().getId());
	}

	@Override
	public Medico encontrarXId(int id) throws Exception {
		log.info("Inicia lógica para encontrar por ID a un médico...");
		return medicoDao.findOne(id);
	}

	@Override
	public List<Medico> encontrarTodos() throws Exception {
		log.info("Inicia lógica para encontrar a todos los médicos...");
		return medicoDao.findAll();
	}

	@Override
	public void eliminarXId(int id) throws Exception {
		// TODO Auto-generated method stub

	}

}
