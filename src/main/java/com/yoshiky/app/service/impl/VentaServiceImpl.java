package com.yoshiky.app.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yoshiky.app.dao.VentaDao;
import com.yoshiky.app.model.Venta;
import com.yoshiky.app.service.VentaService;

@Service
public class VentaServiceImpl implements VentaService {

	private static final Logger log = LoggerFactory.getLogger(VentaServiceImpl.class);

	@Autowired
	private VentaDao ventaDao;

	@Override
	public Venta crear(Venta t) throws Exception {
		log.info("Inicia lógica para crear una venta...");
		t.getVentaDetalle().forEach(x -> x.setVenta(t));
		return ventaDao.save(t);
	}

	@Override
	public List<Venta> encontrarTodos() throws Exception {
		// TODO Auto-generated method stub
		log.info("Inicia lógica para encontrar a todas las ventas...");
		return ventaDao.findAll();
	}

}
