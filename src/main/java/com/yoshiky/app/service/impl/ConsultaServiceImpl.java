package com.yoshiky.app.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yoshiky.app.dao.ConsultaDao;
import com.yoshiky.app.model.Consulta;
import com.yoshiky.app.service.ConsultaService;

@Service
public class ConsultaServiceImpl implements ConsultaService {

	private static final Logger log = LoggerFactory.getLogger(ConsultaServiceImpl.class);

	@Autowired
	private ConsultaDao consultaDao;

	@Override
	public Consulta crear(Consulta t) throws Exception {
		log.info("Inicia lógica para crear una consulta...");
		t.getConsultaDetalle().forEach(x -> x.setConsulta(t));
		return consultaDao.save(t);
	}

	@Override
	public List<Consulta> encontrarTodos() throws Exception {
		log.info("Inicia lógica para encontrar a todas las consultas...");
		return consultaDao.findAll();
	}

}
