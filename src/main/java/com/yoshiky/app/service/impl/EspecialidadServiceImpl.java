package com.yoshiky.app.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yoshiky.app.dao.EspecialidadDao;
import com.yoshiky.app.model.Especialidad;
import com.yoshiky.app.service.EspecialidadService;

@Service
public class EspecialidadServiceImpl implements EspecialidadService {

	private static final Logger log = LoggerFactory.getLogger(EspecialidadServiceImpl.class);

	@Autowired
	private EspecialidadDao especialidadDao;

	@Override
	public Especialidad crear(Especialidad t) throws Exception {
		log.info("Inicia lógica para crear una especialidad...");
		return especialidadDao.save(t);
	}

	@Override
	public Especialidad actualizar(Especialidad t) throws Exception {
		log.info("Inicia lógica para actualizar una especialidad...");
		return especialidadDao.save(t);
	}

	@Override
	public void eliminarXId(int id) throws Exception {
		log.info("Inicia lógica para eliminar una especialidad...");
		especialidadDao.delete(id);
	}

	@Override
	public Especialidad encontrarXId(int id) throws Exception {
		log.info("Inicia lógica para encontrar por ID una especialidad...");
		return especialidadDao.findOne(id);
	}

	@Override
	public List<Especialidad> encontrarTodos() throws Exception {
		log.info("Inicia lógica para encontrar a todas las especialidades...");
		return especialidadDao.findAll();
	}

	@Override
	public void eliminar(Especialidad id) throws Exception {
		// TODO Auto-generated method stub

	}

}
