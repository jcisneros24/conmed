package com.yoshiky.app.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yoshiky.app.dao.ExamenDao;
import com.yoshiky.app.model.Examen;
import com.yoshiky.app.service.ExamenService;

@Service
public class ExamenServiceImpl implements ExamenService {

	private static final Logger log = LoggerFactory.getLogger(ExamenServiceImpl.class);

	@Autowired
	private ExamenDao examenDao;

	@Override
	public Examen crear(Examen t) throws Exception {
		log.info("Inicia lógica para crear un éxamen...");
		return examenDao.save(t);
	}

	@Override
	public Examen actualizar(Examen t) throws Exception {
		log.info("Inicia lógica para actualizar un éxamen...");
		return examenDao.save(t);
	}

	@Override
	public void eliminarXId(int id) throws Exception {
		log.info("Inicia lógica para eliminar un éxamen...");
		examenDao.delete(id);
	}

	@Override
	public Examen encontrarXId(int id) throws Exception {
		log.info("Inicia lógica para encontrar por ID a un éxamen...");
		return examenDao.findOne(id);
	}

	@Override
	public List<Examen> encontrarTodos() throws Exception {
		log.info("Inicia lógica para encontrar todos los éxamenes...");
		return examenDao.findAll();
	}

	@Override
	public void eliminar(Examen id) throws Exception {
		// TODO Auto-generated method stub

	}

}
