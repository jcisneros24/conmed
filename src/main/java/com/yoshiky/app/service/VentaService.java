package com.yoshiky.app.service;

import java.util.List;

import com.yoshiky.app.model.Venta;

public interface VentaService {

	Venta crear(Venta t) throws Exception;

	List<Venta> encontrarTodos() throws Exception;

}
