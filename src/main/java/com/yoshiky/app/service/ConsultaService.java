package com.yoshiky.app.service;

import java.util.List;

import com.yoshiky.app.model.Consulta;

public interface ConsultaService {

	Consulta crear(Consulta t) throws Exception;

	List<Consulta> encontrarTodos() throws Exception;
}
