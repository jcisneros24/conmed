package com.yoshiky.app.service;

import com.yoshiky.app.model.Especialidad;

public interface EspecialidadService extends GenericsService<Especialidad> {

}
