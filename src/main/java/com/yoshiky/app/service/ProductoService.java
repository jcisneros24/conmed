package com.yoshiky.app.service;

import com.yoshiky.app.model.Producto;

public interface ProductoService extends GenericsService<Producto> {

}
