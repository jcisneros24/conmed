package com.yoshiky.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yoshiky.app.model.Consulta;

public interface ConsultaDao extends JpaRepository<Consulta, Integer> {

}
