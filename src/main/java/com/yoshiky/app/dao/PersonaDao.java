package com.yoshiky.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yoshiky.app.model.Persona;

public interface PersonaDao extends JpaRepository<Persona, Integer> {

}
