package com.yoshiky.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yoshiky.app.model.Paciente;

public interface PacienteDao extends JpaRepository<Paciente, Integer> {

}
