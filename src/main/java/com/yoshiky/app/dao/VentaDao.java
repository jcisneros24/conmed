package com.yoshiky.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yoshiky.app.model.Venta;

public interface VentaDao extends JpaRepository<Venta, Integer> {

}
