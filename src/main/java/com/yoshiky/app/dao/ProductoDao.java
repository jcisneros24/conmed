package com.yoshiky.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yoshiky.app.model.Producto;

public interface ProductoDao extends JpaRepository<Producto, Integer> {

}
