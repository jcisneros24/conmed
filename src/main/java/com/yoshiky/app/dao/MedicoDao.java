package com.yoshiky.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yoshiky.app.model.Medico;

public interface MedicoDao extends JpaRepository<Medico, Integer> {

}
