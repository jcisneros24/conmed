package com.yoshiky.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yoshiky.app.model.Especialidad;

public interface EspecialidadDao extends JpaRepository<Especialidad, Integer> {

}
