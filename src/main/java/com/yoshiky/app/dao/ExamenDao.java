package com.yoshiky.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.yoshiky.app.model.Examen;

public interface ExamenDao extends JpaRepository<Examen, Integer> {

}
